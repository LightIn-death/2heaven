extends KinematicBody2D

export var GRAVITY = 30
const FLOOR = Vector2(0, -1)
var velocity = Vector2()
export var speed = 30
export var direction = 1
export var max_walk_duration = 5
export var max_break_duration = 3

var walk_duration_left
var break_duration_left
var is_walking
var rng = RandomNumberGenerator.new()
func _ready():
	rng.randomize()
	
	$AnimationPlayer.play("move")
	
	walk_duration_left = rng.randi_range(1, max_walk_duration)
	is_walking = true
	

func _physics_process(delta):
	if is_on_wall():
		direction *= -1 
		$Sprite.scale.x *= -1
	if rng.randi_range(0,100) == 42:
		direction *= -1 
		$Sprite.scale.x *= -1
	
	if is_walking:
		velocity.x = speed * direction
		walk_duration_left -= delta
		if walk_duration_left <= 0:
			is_walking = false
			break_duration_left = rng.randi_range(1,max_break_duration)
			$AnimationPlayer.stop()
	else:
		break_duration_left -= delta
		velocity.x =0
		if break_duration_left <= 0:
			is_walking = true
			walk_duration_left = rng.randi_range(1,max_walk_duration)
			$AnimationPlayer.play()
	
	
	if is_on_floor():
		velocity.y = 0
	else:
		velocity.y += GRAVITY
	
	velocity =  move_and_slide(velocity, FLOOR)
	
func infect():
	$AnimationPlayer.play("move_infected")
	$Particles2D.emitting = true
