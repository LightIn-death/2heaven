extends KinematicBody2D

export var lifetime = 4
export var speed = 60
var timer
var velocity = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	timer = Timer.new()
	timer.connect("timeout",self, "queue_free")
	timer.set_wait_time(lifetime)
	add_child(timer)
	timer.start()
	
	velocity = (get_global_mouse_position() - position).normalized() * speed

func _physics_process(delta):
	
	var collision = move_and_collide(velocity * delta)
	if collision:
		if collision.collider.has_method('infect'):
			collision.collider.infect()
		queue_free()
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
