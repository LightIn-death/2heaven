extends KinematicBody2D

export var speed = 400
export var gravity = 300
export var jump_force = 4000
export var velocity = Vector2()

func _physics_process(delta):

	if is_on_floor():
		print("FLOOR")
		velocity.y = 0
	else:
		velocity.y += gravity
		
	if Input.is_action_pressed("ui_right"):
		$AnimationPlayer.play("player_walk")
		velocity.x = speed
		$Sprite.flip_h = false
		#$CollisionPolygon2D.scale.x = 1
	if Input.is_action_pressed("ui_left"):
		$AnimationPlayer.play("player_walk")
		velocity.x = - speed
		$Sprite.flip_h = true
		#$CollisionPolygon2D.scale.x = -1
	
	if not Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right"):
		$AnimationPlayer.stop()
		velocity.x = 0
	
	if Input.is_action_just_pressed("ui_up"):
		velocity.y -= jump_force
		
	velocity = move_and_slide(velocity, Vector2(0, -1))
		


	
